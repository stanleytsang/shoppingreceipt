package com.stanley.test.shopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stanley.test.shopping.entity.LocationProductCategoryTaxExempt;
import com.stanley.test.shopping.entity.LocationProductCategoryTaxExemptID;

@Repository
public interface LocationProductCategoryTaxExemptRepository extends JpaRepository<LocationProductCategoryTaxExempt, LocationProductCategoryTaxExemptID>{

}
