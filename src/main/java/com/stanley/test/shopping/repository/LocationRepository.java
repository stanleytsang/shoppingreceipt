package com.stanley.test.shopping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stanley.test.shopping.entity.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Integer>{

}
