package com.stanley.test.shopping.config;

import java.util.EnumSet;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnableWithStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import com.stanley.test.shopping.model.StateMachineModel;
import com.stanley.test.shopping.model.StateMachineModel.States;


@Configuration
@EnableStateMachine 
@Profile("!test")
public class SpringConfig extends EnumStateMachineConfigurerAdapter<StateMachineModel.States, StateMachineModel.Events>{

	
	@Override
    public void configure(StateMachineConfigurationConfigurer<StateMachineModel.States, StateMachineModel.Events> config)
            throws Exception {
        config
            .withConfiguration()
                .autoStartup(true);
    }
	
	@Override
    public void configure(StateMachineStateConfigurer<StateMachineModel.States, StateMachineModel.Events> states)
            throws Exception {
        states
            .withStates()
                .initial(States.INIT)
                .states(EnumSet.allOf(StateMachineModel.States.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<StateMachineModel.States, StateMachineModel.Events> transitions)
            throws Exception {
        transitions
	        .withExternal()
		        .source(States.INIT).target(States.INIT)
		        .event(StateMachineModel.Events.INIT)
		        .and()
            .withExternal()
                .source(States.INIT).target(States.MAIN_MENU)
                .event(StateMachineModel.Events.LOCATION_SELECTED)
                .and()
            .withExternal()
                .source(States.MAIN_MENU).target(States.PRODUCT)
                .event(StateMachineModel.Events.GO_ADD_PRODUCT)
                .and()
            .withExternal()
                .source(States.PRODUCT).target(States.MAIN_MENU)
                .event(StateMachineModel.Events.ADDED_PRODUCT)
                .and()
            .withExternal()
                .source(States.MAIN_MENU).target(States.CHECKOUT)
                .event(StateMachineModel.Events.CHECKOUT)
                .and()
	        .withExternal()
		        .source(States.CHECKOUT).target(States.INIT)
		        .event(StateMachineModel.Events.INIT);
    }

	

}
