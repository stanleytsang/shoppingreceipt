package com.stanley.test.shopping.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="location_product_category_tax_exempt")
public class LocationProductCategoryTaxExempt {

	@EmbeddedId
	private LocationProductCategoryTaxExemptID id;
	
	
	@Column(name="tax_exempt")
	private boolean taxExempt;


	public LocationProductCategoryTaxExemptID getId() {
		return id;
	}


	public void setId(LocationProductCategoryTaxExemptID id) {
		this.id = id;
	}


	public boolean isTaxExempt() {
		return taxExempt;
	}


	public void setTaxExempt(boolean taxExempt) {
		this.taxExempt = taxExempt;
	}
	
}
