package com.stanley.test.shopping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.stanley.test.shopping.controller.IShoppingCartControllerTest;

@SpringBootApplication
public class ShoppingCartTestApplication implements CommandLineRunner{

	@Autowired
	IShoppingCartControllerTest controller;
	
	public static void main(String[] args) {
		SpringApplication.run(ShoppingCartTestApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		controller.start();
		
	}

}
