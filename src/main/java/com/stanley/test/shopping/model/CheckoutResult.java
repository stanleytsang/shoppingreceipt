package com.stanley.test.shopping.model;

import java.util.ArrayList;
import java.util.List;

public class CheckoutResult {	

	private List<LineItem> lineItems = new ArrayList<>();
	
	private Double subTotal;
	
	private Double tax;

	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}
	
	
	public Double getTotal() {
		return Math.round((getTax() + getSubTotal()) * 100.0) / 100.0;
	}

	
}
