package com.stanley.test.shopping.model;

import com.stanley.test.shopping.entity.Product;

public class LineItem {

	private Product product;
	
	private Integer quantity;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}
