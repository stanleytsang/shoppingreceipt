package com.stanley.test.shopping.model;

public class StateMachineModel {

	public static enum States {
	    INIT, MAIN_MENU, PRODUCT, CHECKOUT
	}

	public static enum Events {
		INIT, LOCATION_SELECTED, GO_ADD_PRODUCT, ADDED_PRODUCT, CHECKOUT
	}
	
}
