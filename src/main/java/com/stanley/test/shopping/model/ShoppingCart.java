package com.stanley.test.shopping.model;

import java.util.ArrayList;
import java.util.List;

import com.stanley.test.shopping.entity.Location;

public class ShoppingCart {
	
	private Location location;	
	
	private List<LineItem> lineItems = new ArrayList<>();

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

	
}
