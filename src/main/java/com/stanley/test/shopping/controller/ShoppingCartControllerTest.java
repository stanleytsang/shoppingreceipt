package com.stanley.test.shopping.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;
import org.springframework.stereotype.Service;

import com.stanley.test.shopping.entity.Location;
import com.stanley.test.shopping.entity.Product;
import com.stanley.test.shopping.model.CheckoutResult;
import com.stanley.test.shopping.model.StateMachineModel;
import com.stanley.test.shopping.repository.LocationRepository;
import com.stanley.test.shopping.repository.ProductRepository;
import com.stanley.test.shopping.service.ShoppingCartService;


@WithStateMachine
@Profile("test")
public class ShoppingCartControllerTest implements IShoppingCartControllerTest{


	public void goNext() {
	}
	
	public void start() {
	}
	
	
	public void toInit() throws IOException {
             
    }
	
	
	public  void toMainMenu() throws IOException {
        
    }
	
	public void toProduct() throws IOException {
    }
	
	
	
	public void toCheckout() throws IOException {
    }
	
}
