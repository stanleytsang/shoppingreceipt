package com.stanley.test.shopping.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

import com.stanley.test.shopping.entity.Location;
import com.stanley.test.shopping.entity.Product;
import com.stanley.test.shopping.model.CheckoutResult;
import com.stanley.test.shopping.model.StateMachineModel;
import com.stanley.test.shopping.repository.LocationRepository;
import com.stanley.test.shopping.repository.ProductRepository;
import com.stanley.test.shopping.service.ShoppingCartService;


@WithStateMachine
@Profile("!test")
public class ShoppingCartController implements IShoppingCartControllerTest {

	@Autowired
	ShoppingCartService shoppingCartService;
	@Autowired
	LocationRepository locationRepository;
	

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
    StateMachine<StateMachineModel.States, StateMachineModel.Events> stateMachine;
	
	
	private StateMachineModel.Events next;

	public void goNext() {
		stateMachine.sendEvent(next);
	}
	
	public void start() {
		stateMachine.sendEvent(StateMachineModel.Events.INIT);
		while (true) {
			goNext();
		}
	}
	
	
	@OnTransition(target = "INIT")
	@Transactional
	public void toInit() throws IOException {
		System.out.println("Please select location:");		
		List<Location> locations = locationRepository.findAll();
		
		locations.stream().forEach(loc -> {
			System.out.println(loc.getLocationId() + ": "+ loc.getLocationName()+" ("+loc.getLocationCode()+")");		
		});
		
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        String options = reader.readLine();
        
        int id = Integer.parseInt(options);
        
        shoppingCartService.setLocation(id);
        
        this.next = StateMachineModel.Events.LOCATION_SELECTED;
             
    }
	
	
	
	@OnTransition(target = "MAIN_MENU")
	@Transactional
	public  void toMainMenu() throws IOException {
		System.out.println("Please select Options:");		
		System.out.println("1: Add Product");
		System.out.println("2: Checkout");
		
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        String options = reader.readLine();
        
        if (options.equals("1")) {
        	this.next = StateMachineModel.Events.GO_ADD_PRODUCT;
        }
        else if (options.equals("2")){
        	this.next = StateMachineModel.Events.CHECKOUT;
        }
        
    }
	
	@OnTransition(target = "PRODUCT")
	@Transactional
	public void toProduct() throws IOException {
		System.out.println("Please select product:");	
		List<Product> products = productRepository.findAll();
		products.stream().forEach(product -> {
			System.out.println(product.getProductId() + ": "+ product.getProductName() +" ($"+product.getPrice()+")");		
		});
		
		 BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        String options = reader.readLine();        
        int id = Integer.parseInt(options);
        
        System.out.println("Please input quantity:");	
        options = reader.readLine();        
        int qty = Integer.parseInt(options);
        
        shoppingCartService.addLineItems(id, qty);

    	this.next = StateMachineModel.Events.ADDED_PRODUCT;
    }
	
	
	
	@OnTransition(target = "CHECKOUT")
	@Transactional
	public void toCheckout() throws IOException {
		CheckoutResult result = shoppingCartService.checkout();        
		shoppingCartService.printResult(result);
		this.next = StateMachineModel.Events.INIT;
    }
	
}
