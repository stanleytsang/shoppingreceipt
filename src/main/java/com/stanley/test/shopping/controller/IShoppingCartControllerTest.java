package com.stanley.test.shopping.controller;

import java.io.IOException;

public interface IShoppingCartControllerTest {

	public void goNext();
	
	public void start();
	
	
	public void toInit() throws IOException;
	
	
	public  void toMainMenu() throws IOException;
	
	public void toProduct() throws IOException;
	
	public void toCheckout() throws IOException;
}
