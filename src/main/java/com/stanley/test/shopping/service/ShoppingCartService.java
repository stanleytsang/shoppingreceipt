package com.stanley.test.shopping.service;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stanley.test.shopping.entity.LocationProductCategoryTaxExempt;
import com.stanley.test.shopping.entity.LocationProductCategoryTaxExemptID;
import com.stanley.test.shopping.entity.Product;
import com.stanley.test.shopping.model.CheckoutResult;
import com.stanley.test.shopping.model.LineItem;
import com.stanley.test.shopping.model.ShoppingCart;
import com.stanley.test.shopping.repository.LocationProductCategoryTaxExemptRepository;
import com.stanley.test.shopping.repository.LocationRepository;
import com.stanley.test.shopping.repository.ProductRepository;

@Service
public class ShoppingCartService {

	
	ShoppingCart cart = new ShoppingCart();	
	
	@Autowired
	private LocationRepository locationRepository;	

	@Autowired
	private ProductRepository productRepository;
	

	@Autowired
	private LocationProductCategoryTaxExemptRepository locationProductCategoryTaxExemptRepository;
	

	
	public ShoppingCart getCart() {
		return cart;
	}
	
	public void clearCart() {
		cart = new ShoppingCart();
	}
		
	public void setLocation (int locationId) {
		cart.setLocation(locationRepository.getOne(locationId));
	}
	

	public void addLineItems (int productId, int qty) {
		Product product = productRepository.getOne(productId);
		LineItem item = new LineItem();
		item.setProduct(product);
		item.setQuantity(qty);
		cart.getLineItems().add(item);		
	}
	
	public void printResult(CheckoutResult result) {
		
		StringBuffer header = new StringBuffer();
		header.append(StringUtils.rightPad("item", 20));
		header.append(StringUtils.rightPad("price", 20));
		header.append("qty");
		System.out.println();
		
		System.out.println(StringUtils.repeat("-", 43));
		System.out.println(header.toString());
		System.out.println();
				
		result.getLineItems().stream().forEach(item ->{
			
			String name = item.getProduct().getProductName();			
			String price = "$"+to2Decimal(item.getProduct().getPrice());
			String qty = String.valueOf(item.getQuantity());
			
			int priceLength = 20 - name.length() + 5;
			int qtyLength = 18;
			
			StringBuffer lineStr = new StringBuffer();
			lineStr.append(name);
			lineStr.append(StringUtils.leftPad(price, priceLength));
			lineStr.append(StringUtils.leftPad(qty, qtyLength));
			
			System.out.println(lineStr.toString());
			
		});		
		
		System.out.println("subtotal:" + StringUtils.leftPad("$"+to2Decimal(result.getSubTotal()), 34));
		System.out.println("tax:" + StringUtils.leftPad("$"+to2Decimal(result.getTax()), 39));
		System.out.println("total:" + StringUtils.leftPad("$"+to2Decimal(result.getTotal()), 37));		

		System.out.println(StringUtils.repeat("-", 43));
		System.out.println();
	}
	
	private String to2Decimal(Double number) {
		return String.format("%.2f", number);
	}
	

	public CheckoutResult checkout() {
		Double subTotal = computeSubTotal();
		Double tax = computeTotalTax();
		CheckoutResult result = new CheckoutResult();
		result.setLineItems(cart.getLineItems());

		result.setSubTotal(subTotal);
		result.setTax(tax);		
		
		this.clearCart();
		return result;
	}	
	
	public Double computeSubTotal() {
		Double total = cart.getLineItems().stream().reduce(0.0, (subTotal, item)-> subTotal + (item.getProduct().getPrice() * item.getQuantity()), Double::sum);
		return Math.round(total * 100.0) / 100.0;
	}

	public Double computeTotalTax() {
		Double taxableAmount = cart.getLineItems().stream().reduce(0.0, (subTotal, item)->{
			Product product = item.getProduct();
			LocationProductCategoryTaxExemptID id = new LocationProductCategoryTaxExemptID();
			id.setLocationId(cart.getLocation().getLocationId());
			id.setProductCategoryId(product.getProductCategory().getProductCategoryId());			
			LocationProductCategoryTaxExempt exempt= locationProductCategoryTaxExemptRepository.getOne(id);
			if (exempt.isTaxExempt()) {
				return subTotal;
			}			
			return subTotal + product.getPrice() * item.getQuantity();
			
		},Double::sum);
		
		return this.computeTax(taxableAmount, cart.getLocation().getTaxRate());
	}
	
	
	public Double computeTax(Double taxableAmount, Double rate) {
		double tax = taxableAmount * rate;
		return Math.ceil(tax * 20) / 20.0;		
	}
	
}
