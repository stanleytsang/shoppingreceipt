insert into product_category(product_category_id, product_category_name) values 
(1, 'Stationery'),
(2, 'Food'),
(3, 'Clothing');


insert into location(location_id, location_code, location_name, tax_rate) values 
(1, 'CA', 'California', 0.0975),
(2, 'NY', 'New York', 0.08875);


insert into product (product_id, product_name, price, product_category_id) values
(1, 'book', 17.99, 1),
(2, 'potato chips', 3.99, 2),
(3, 'pencil', 2.99, 1),
(4, 'shirt', 29.99, 3); 


insert into location_product_category_tax_exempt (location_id, product_category_id, tax_exempt) values 
(1, 1, false),
(1, 2, true),
(1, 3, false),

(2, 1, false),
(2, 2, true),
(2, 3, true);
