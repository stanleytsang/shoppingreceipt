package com.stanley.test.shopping;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.stanley.test.shopping.entity.Location;
import com.stanley.test.shopping.entity.Product;
import com.stanley.test.shopping.model.CheckoutResult;
import com.stanley.test.shopping.model.LineItem;
import com.stanley.test.shopping.model.ShoppingCart;
import com.stanley.test.shopping.service.ShoppingCartService;

@SpringBootTest
@ActiveProfiles("test")
class ShoppingCartTestApplicationTests {

	@Test
	void contextLoads() {
	}

	
	@Autowired 
	ShoppingCartService shoppingCartService;
	
	

	@Test
	public void testComputeTax1() {
		Double tax =shoppingCartService.computeTax(1.13, 1.0);		
		assertEquals(tax, 1.15);
	}
	
	@Test
	public void testComputeTax2() {
		Double tax =shoppingCartService.computeTax(1.16, 1.0);		
		assertEquals(tax, 1.20);
	}
	
	@Test
	public void testComputeTax3() {
		Double tax =shoppingCartService.computeTax(1.151, 1.0);		
		assertEquals(tax, 1.20);
	}
	
	
	
	
	@Test
	@Transactional
	public void testAddProduct() {
		shoppingCartService.addLineItems(1, 2);		
		ShoppingCart cart = shoppingCartService.getCart();		
		LineItem item = cart.getLineItems().get(0);		
		assertNotNull(item.getProduct());			
		
		Product product = item.getProduct();
		assertEquals(product.getProductId(), 1);
		assertEquals(product.getProductCategory().getProductCategoryId(), 1);
		assertEquals(item.getQuantity(), 2);	
		
	}
	
	@Test
	@Transactional
	public void testSetLocation() {
		shoppingCartService.setLocation(1);
		ShoppingCart cart = shoppingCartService.getCart();		
		Location location = cart.getLocation();
		assertEquals(location.getLocationId(), 1);
		assertEquals(location.getLocationCode(), "CA");
		
	}
	
	
	@Test
	@Transactional
	public void testUseCase1() {
		shoppingCartService.setLocation(1);
		shoppingCartService.addLineItems(1, 1);
		shoppingCartService.addLineItems(2, 1);
		
		CheckoutResult result = shoppingCartService.checkout();
		
		assertEquals(result.getSubTotal(), 21.98);
		assertEquals(result.getTax(), 1.80);
		assertEquals(result.getTotal(), 23.78);
		
	}
	
	
	@Test
	@Transactional
	public void testUseCase2() {
		shoppingCartService.setLocation(2);
		shoppingCartService.addLineItems(1, 1);
		shoppingCartService.addLineItems(3, 3);
		
		CheckoutResult result = shoppingCartService.checkout();
		
		assertEquals(result.getSubTotal(), 26.96);
		assertEquals(result.getTax(), 2.40);
		assertEquals(result.getTotal(), 29.36);
		
	}
	
	
	@Test
	@Transactional
	public void testUseCase3() {
		shoppingCartService.setLocation(2);
		shoppingCartService.addLineItems(3, 2);
		shoppingCartService.addLineItems(4, 1);
		
		CheckoutResult result = shoppingCartService.checkout();
		
		assertEquals(result.getSubTotal(), 35.97);
		assertEquals(result.getTax(), 0.55);
		assertEquals(result.getTotal(), 36.52);
		
	}
	
	
}
